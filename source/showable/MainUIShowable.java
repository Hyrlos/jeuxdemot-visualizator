/*
 * Main.java                                11 juin 2018
 * Stagiaire LIRMM 2018
 */
package source.showable;

import java.awt.EventQueue;

/**
 * Lancement de l'application
 * @author Thomas GEORGES 
 *
 */
public class MainUIShowable {

	/**
	 * Launch the application.
	 * @param args unused
	 */
	public static void main(String[] args) {
		System.out.println("Début Appli");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					InterfaceFiltres frame = new InterfaceFiltres();
					frame.setVisible(true);
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

}
