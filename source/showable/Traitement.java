/*
 * Traitement.java                                7 juin 2018
 * Stagiaire LIRMM 2018
 */
package source.showable;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.IllegalFormatCodePointException;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;

import requeterRezo.Filtre;
import requeterRezo.Mot;
import requeterRezo.RequeterRezo;
import requeterRezo.Voisin;

/**
 * Traitement et mise en relation des données Puis affichage et gestion du
 * graphe
 * 
 * @author Thomas GEORGES
 *
 */
public class Traitement {
	/** Objet manipulant les filtres */
	private Filtres filtres;
	/** Objet contenant la structure du graphe de JDM */
	private GraphJDM graph;
	/** Requêtteur de rezoJDM */
	private RequeterRezo systeme;
	/** Structure avec la liste des types de relations de rezoJDM */
	private ListeTypeRelation listeRelation;
	/** tableau contenant les relations à afficher format (n1, t, n2, w) */
	private ArrayList<String[]> touteRelationAffi;
	/** tableau contenant les relations format (n1, t, n2, w) */
	private ArrayList<String[]> touteRelation;
	/** tableau contenant les raffinement format (n1, t, n2, w) */
	private ArrayList<String[]> raffRelation;
	/** tableau contenant les idées associées format (n1, t, n2, w) */
	private ArrayList<String[]> ideeAssocRelation;
	/** tableau contenant les node format (n1, w) */
	private ArrayList<String[]> node;

	Traitement() {
		graph = new GraphJDM();
	}

	/**
	 * Constructeur
	 * 
	 * @param filtres
	 *            Object filtre contenant les filtres à appliquer
	 * @return
	 * @throws MalformedURLException
	 *             Si pb avec l'URL
	 * @throws problemeRelationException
	 *             Si problème avec la génération des types de realtions
	 * @throws ExceptionJDM
	 *             Si mot incorrect ou JDM off
	 * @throws IOException
	 *             PB affichage
	 * @throws InterruptedException
	 *             PB thread
	 */
	void TraitementAvecFiltre(Filtres filtres)
			throws MalformedURLException, problemeRelationException, ExceptionJDM, IOException, InterruptedException {

		resetGraph();

		this.filtres = filtres;

		graph = graph.graphJDM(filtres.getTerme(), this);
		TraitementFiltre();
	}

	void TraitementLast() throws IllegalFormatCodePointException {
		resetGraph();
		System.out.println("New graph");
		graph = graph.graphJDM(this);

		this.filtres = new Filtres();

		systeme = new RequeterRezo();
	}

	/**
	 * ReInitialisation des tableaux de relations
	 */
	private void reInit() {
		/* Initialisations */
		touteRelationAffi = new ArrayList<String[]>();
		touteRelation = new ArrayList<String[]>();
		ideeAssocRelation = new ArrayList<String[]>();
		raffRelation = new ArrayList<String[]>();
		node = new ArrayList<String[]>();
	}

	/**
	 * traitement et mise en relation des données et du graphe
	 * 
	 * @param filtres
	 *            la structure contenant les filtres
	 * @throws problemeRelationException
	 *             Si la liste des types de relations est mal généré
	 * @throws ExceptionJDM
	 *             Si problème sur le requêtteur
	 * @throws MalformedURLException
	 *             Problème d'URL
	 * @throws IOException
	 *             Problème d'affichage
	 * @throws InterruptedException
	 *             Problème ?
	 */
	public void TraitementFiltre()
			throws problemeRelationException, ExceptionJDM, MalformedURLException, IOException, InterruptedException {

		System.out.println("Traitement : ");

		/* Initialisations */
		touteRelationAffi = new ArrayList<String[]>();
		touteRelation = new ArrayList<String[]>();
		ideeAssocRelation = new ArrayList<String[]>();
		raffRelation = new ArrayList<String[]>();
		node = new ArrayList<String[]>();

		listeRelation = new ListeTypeRelation();

		systeme = new RequeterRezo();

		/* Affichage des filtres */
		System.out.println("\n\tFiltres :");
		System.out.println("Mot : " + filtres.getTerme());
		System.out.println("Relations " + (filtres.isRelationEntrante(0) ? "sortante" : "entrante"));
		String[][] tab1 = filtres.getRelationHauteur();
		int[] tab = filtres.getNbVoisin();
		for (int i = 0; i < filtres.getNb_nbVoisin(); i++) {
			System.out.print("Voisin de distance " + (i + 1) + " : " + tab[i] + " avec la relation");
			for (int u = 0; u < tab1[i].length; u++) {
				System.out.print(" - " + listeRelation.getRelById(Integer.parseInt(tab1[i][u])));
			}
			System.out.println("");
		}

		System.out.println("\n\tRelation :");
		/* Rempli le tableau des relations */
		remplirRelation();

		System.out.println("\n Insertion graphe");
		affichage();

		/* ajoute au tableau des relations les relations de raffinement utile */
		System.out.println("\n\t Rafinement");
		insertionRaff_ideeAssoc();

		System.out.println("\n Insertion graphe");
		affichage();

		/* Pour JDM */
		systeme.sauvegarder();

		System.out.println("\n Affichage graph");
		graph.affichage();
	}

	/**
	 * Remplissage du tableau relation à afficher à partir d'un mot et de sa
	 * hauteur
	 * 
	 * @param mot
	 *            mot racine
	 * @param nb
	 *            nombre de relation à ajouter au tableau
	 * @param hauteur
	 *            le niveau du graphe racine = 0 relation 1er ordre = 1 ...
	 * @throws ExceptionJDM
	 *             Si JDM off ou mot incorrect
	 */
	private void remplirRelation() throws ExceptionJDM {

		new ArrayList<String[]>();
		ArrayList<Voisin> listeRelationType0 = new ArrayList<Voisin>();
		ArrayList<Voisin> listeRelationType1 = new ArrayList<Voisin>();
		HashMap<Integer, ArrayList<Voisin>> listeEntree = new HashMap<Integer, ArrayList<Voisin>>();

		String[] nodeCourante = { filtres.getTerme(), "0" };
		node.add(nodeCourante);

		/* Pour toutes les nodes */
		for (int i = 0; i < node.size(); i++) {
			ArrayList<String[]> relations = new ArrayList<String[]>();
			int hauteur = Integer.parseInt(node.get(i)[1]);
			if (hauteur < filtres.getNb_nbVoisin()) {
				String mot = node.get(i)[0];
				boolean isRelationEntrante = filtres.isRelationEntrante(hauteur);

				// String idRelation =
				// listeRelation.getRelById(Integer.parseInt(filtres.getRelation(hauteur)));
				String[] temptabidRelation = filtres.getRelation(hauteur);
				String[] idRelation = new String[temptabidRelation.length];
				for (int y = 0; y < temptabidRelation.length; y++) {
					idRelation[y] = listeRelation.getRelById(Integer.parseInt(temptabidRelation[y]));
				}

				ArrayList<Voisin>[] listeRelationTypeChoix = new ArrayList[temptabidRelation.length];

				Mot current = null;
				try {
					current = systeme.requete(mot);
				} catch (Exception e) {
					System.err.println(e + "\n erreur mot");
				}

				if (i == 0) {
					if (current == null) {
						return;
					}
					Node rac = graph.addRacine(filtres.getTerme());
					GraphJDM.addClass(rac, "racine");
				}

				if (current != null) {

					/* On prends les relations entrantes OU sortantes */
					if (isRelationEntrante) {
						listeEntree = current.getRelations_entrantes();
					} else {
						listeEntree = current.getRelations_sortantes();
					}

					/* Pour toutes les relations */
					// Relation courante u
					listeRelationType0 = listeEntree.get(0);
					listeRelationType1 = listeEntree.get(1);
					/* TODO concat pour avoir plusieurs relations */
					for (int y = 0; y < temptabidRelation.length; y++) {
						listeRelationTypeChoix[y] = listeEntree.get(listeRelation.getIDByRel(idRelation[y]));
					}

					// si la relation est sementique
					if (listeRelationType1 != null) {
						for (int v = 0; v < listeRelationType1.size(); v++) {
							String n2 = isRelationEntrante ? mot : listeRelationType1.get(v).getNom();
							String n1 = isRelationEntrante ? listeRelationType1.get(v).getNom() : mot;
							String[] tempo = { n1, listeRelation.getRelById(1), n2,
									"" + listeRelationType1.get(v).getPoids() };
							raffRelation.add(tempo);
						}
					}

					// si la relation est idée associée
					if (listeRelationType0 != null) {
						for (int v = 0; v < listeRelationType0.size(); v++) {
							String n2 = isRelationEntrante ? mot : listeRelationType0.get(v).getNom();
							String n1 = isRelationEntrante ? listeRelationType0.get(v).getNom() : mot;
							String[] tempo = { n1, listeRelation.getRelById(0), n2,
									"" + listeRelationType0.get(v).getPoids() };
							ideeAssocRelation.add(tempo);
						}
					}
					// pour tout les relations
					for (int y = 0; y < listeRelationTypeChoix.length; y++) {
						// si la relation est celle recherché
						if (listeRelationTypeChoix[y] != null) {
							// Pour chaque mot avec cette relation
							for (int v = 0; v < listeRelationTypeChoix[y].size(); v++) {
								String n2 = isRelationEntrante ? mot : listeRelationTypeChoix[y].get(v).getNom();
								String n1 = isRelationEntrante ? listeRelationTypeChoix[y].get(v).getNom() : mot;
								String[] tempo = { n1, idRelation[y], n2,
										"" + listeRelationTypeChoix[y].get(v).getPoids() };

								relations.add(tempo);
							}
						}
						TrieEtAjoutList(relations, filtres.getNbVoisinIndice(hauteur), hauteur);
					}
				}
			}
		}
	}

	/**
	 * Trie les relation de aTrier. En trie NB dans touteRelationAffi Et le
	 * reste dans touteRelation
	 * 
	 * @param aTrier
	 *            tableau a trier
	 * @param nb
	 *            le nombre à trier
	 * @param hauteur
	 *            la hauteur des relations
	 */
	private void TrieEtAjoutList(ArrayList<String[]> aTrier, int nb, int hauteur) {
		for (int u = 0; u < aTrier.size() && u < nb && aTrier.size() > 0; u++) {
			int indice = 0;

			for (int i = 0; i < aTrier.size(); i++) {

				if (Math.abs(Double.parseDouble(aTrier.get(i)[3])) > Math.abs(Double.parseDouble(aTrier.get(indice)[3]))
						&& aTrier.get(i)[0].charAt(0) != '=' && aTrier.get(i)[2].charAt(0) != '='
						&& aTrier.get(i)[0].charAt(0) != '_' && aTrier.get(i)[2].charAt(0) != '_') {
					indice = i;
				}
			}

			touteRelationAffi.add(aTrier.get(indice));
			System.out.println(aTrier.get(indice)[0] + " " + aTrier.get(indice)[1] + " " + aTrier.get(indice)[2] + " "
					+ aTrier.get(indice)[3]);
			String mot = filtres.isRelationEntrante(hauteur) ? aTrier.get(indice)[0] : aTrier.get(indice)[2];
			String[] laNode = { mot, "" + (hauteur + 1) };
			node.add(laNode);

			aTrier.remove(indice);
		}

		for (int i = 0; i < aTrier.size(); i++) {
			touteRelation.add(aTrier.get(i));
		}
	}

	/**
	 * 
	 */
	private void affichage() {
		/* Création du graph */
		for (int i = 0; i < touteRelationAffi.size(); i++) {
			String[] laRelation = touteRelationAffi.get(i);

			if (laRelation[0].contains(">")) {
				laRelation[0] = raff(laRelation[0]);
			}

			if (laRelation[2].contains(">")) {
				laRelation[2] = raff(laRelation[2]);
			}

			Edge arcCourant;
			if (laRelation[1].equals("r_associated")) {
				arcCourant = graph.addRelation(laRelation[0], laRelation[1] + " " + laRelation[3], laRelation[2]);
			} else {
				arcCourant = graph.addRelation(laRelation[0], laRelation[1] + " " + laRelation[3], laRelation[2]);
				if (arcCourant != null) {
					// arcCourant.addAttribute("ui.label", laRelation[1] + " " +
					// laRelation[3]);
				}
			}

			System.out.println(
					laRelation[0] + " - " + laRelation[1] + " " + tailleEdge(laRelation[3]) + " - " + laRelation[2]);

			if (arcCourant != null) {
				GraphJDM.addClass(arcCourant, laRelation[1]);
				arcCourant.addAttribute("ui.style", "size : " + tailleEdge(laRelation[3]) + ";");
				arcCourant.addAttribute("ui.style",
						"fill-color: " + listeRelation.colorToRel("" + listeRelation.getIDByRel(laRelation[1])) + ";");
				// arcCourant.addAttribute("ui.style", "arrow-size : " +
				// tailleEdge(laRelation[3]) + ";");
			}
		}

		touteRelationAffi = new ArrayList<String[]>();
	}

	/**
	 * Renvoi une forme non formaté d'un rafinement initial mot>idRaff final
	 * mot>raff
	 * 
	 * @param leMot
	 *            rafiné
	 * @return un mot lisible
	 */
	private String raff(String leMot) {
		Mot mot = systeme.requete(leMot, Filtre.FiltreRelationsEntrantesSortantes);
		if (mot == null) {
			return leMot;
		} // else
		return mot.getMotFormate();
	}

	/**
	 * Renvoi une taille afin de formaté les arretes
	 * 
	 * @param poid
	 *            le poid de la relation
	 * @return le taille de l'arête
	 */
	private static String tailleEdge(String poid) {
		int max = 300;
		double taille = Math.abs(Double.parseDouble(poid)) + 0.5;
		String tailleS = taille > max ? "3,0" : "" + (taille * (2.0 / max) + 1);
		return tailleS.replace(".", ",");
	}

	/**
	 * insert les relations de raffinement si la node de départ et d'arrivée
	 * existe
	 */
	private void insertionRaff_ideeAssoc() {

		System.out.println("Nb raff " + raffRelation.size());
		for (int i = 0; i < raffRelation.size(); i++) {

			String[] tempo = raffRelation.get(i);

			if (tempo[0].contains(">")) {
				tempo[0] = raff(tempo[0]);
			}

			if (tempo[2].contains(">")) {
				tempo[2] = raff(tempo[2]);
			}

			if (graph.isNode(tempo[0]) && graph.isNode(tempo[2])) {
				touteRelationAffi.add(tempo);
				System.out.println(tempo[0] + " " + tempo[1] + " " + tempo[2]);
			}
		}

		if (filtres.isIdeeAssoc()) {
			System.out.println("Nb relation assoc " + ideeAssocRelation.size());
			for (int i = 0; i < ideeAssocRelation.size(); i++) {

				String[] tempo = ideeAssocRelation.get(i);

				if (graph.isNode(tempo[0]) && graph.isNode(tempo[2])) {
					touteRelationAffi.add(tempo);
					// System.out.println(tempo[0] + " " + tempo[1] + " " +
					// tempo[2]);
				}
			}
		}
	}

	/**
	 * Lance un nouveau traitement sur la nouvelle racine
	 * 
	 * @param id
	 *            id de la nouvelle racine
	 * @throws ExceptionJDM
	 *             si JDM down ou pb avec mot
	 */
	public void addRacine(String id) throws ExceptionJDM {
		reInit();

		System.out.println("Add new racine " + id);
		filtres.setTerme(id);
		System.out.println("\n\tRelation : " + id);
		/* Rempli le tableau des relations */
		remplirRelation();

		System.out.println("\n Insertion graphe");
		affichage();

		/* ajoute au tableau des relations les relations de raffinement utile */
		System.out.println("\n\t Rafinement");
		insertionRaff_ideeAssoc();

		System.out.println("\n Insertion graphe");
		affichage();

		/* Pour JDM */
		systeme.sauvegarder();
	}

	private void resetGraph() {
		graph.reset();
	}

	/**
	 * TODO commenter le rôle de la méthode
	 * 
	 */
	public void saveFiltres() {
		ObjectOutputStream oos = null;
		try {
			final FileOutputStream fichier = new FileOutputStream("filtres.ser");
			oos = new ObjectOutputStream(fichier);
			oos.writeObject(filtres);
			oos.flush();
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			try {
				if (oos != null) {
					oos.flush();
					oos.close();
				}
			} catch (final IOException ex) {
				ex.printStackTrace();
			}
		}
		System.out.println("Sérialisé");
	}
}