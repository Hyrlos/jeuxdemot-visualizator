/*
 * MainConsole.java                                21 juin 2018
 * Stagiaire LIRMM 2018
 */
package source.showable;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import requeterRezo.Mot;
import requeterRezo.RequeterRezo;

/**
 * TODO commenter la responsabilité de la classe
 * 
 * @author Thomas GEORGES
 *
 */
public class MainConsole {

	private static boolean boolTerme;
	private static boolean boolNbVoisinNiveau;
	private static boolean boolTypeRelNiveau;
	private static boolean boolRelationEntrante;
	private static boolean boolIdeeAssoc;

	private static String terme;
	private static boolean ideeAssoc;

	/**
	 * Analyse des parametres : // mot -t terme
	 * 
	 * // nombre de voisin par niveau -v n1 n2 n3 ...
	 * 
	 * // Type de relation par niveau - ? -r n1 n2 n3
	 * 
	 * // si relation entrante -e true -e false
	 * 
	 * // si idée associé -ia true -ia false
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		

		Traitement trait = new Traitement();
		
		if (args.length == 0 || args[0].equals("-h") || args[0].equals("--help")) {
			affHelp();
			return;
		}
		terme = "";

		ArrayList<Integer> arrayNbVoisinNiveau = new ArrayList<Integer>();
		ArrayList<String> arraytypeRelNiveau = new ArrayList<String>();
		ArrayList<Boolean> arrayRelationEntrante = new ArrayList<Boolean>();

		for (int i = 0; i < args.length; i++) {
			if (args[i].charAt(0) == '-') {
				setBoolean(args[i]);
			} else {
				if (boolTerme) {
					terme += args[i];
				} else if (boolNbVoisinNiveau) {
					arrayNbVoisinNiveau.add(Integer.parseInt(args[i]));
				} else if (boolTypeRelNiveau) {
					arraytypeRelNiveau.add(args[i]);
				} else if (boolRelationEntrante) {
					arrayRelationEntrante.add(Boolean.parseBoolean(args[i]));
				} else if (boolIdeeAssoc) {
					ideeAssoc = (Boolean.parseBoolean(args[i]));
				}
			}
		}
		boolean ok = true;

		Filtres filtres = new Filtres();

		/* TERME */
		if (verifMot(terme)) {
			filtres.setTerme(terme);
		} else {
			System.err.println("Erreur avec terme");
			return;
		}

		/* Nombre voisins */
		int[] nbVoisin = new int[arrayNbVoisinNiveau.size()];
		for (int i = 0; i < arrayNbVoisinNiveau.size(); i++) {
			nbVoisin[i] = arrayNbVoisinNiveau.get(i);
		}
		filtres.setNbVoisin(nbVoisin);

		/* Relation hauteur */
		String[][] relationHauteur = new String[arrayNbVoisinNiveau.size()][1];
		for (int i = 0; i < arrayNbVoisinNiveau.size(); i++) {
			relationHauteur[i][0] = i < arraytypeRelNiveau.size() ? arraytypeRelNiveau.get(i) : "0";
		}

		if (verifEtTraitRelation(relationHauteur)) {
			filtres.setRelationHauteur(relationHauteur);
		} else {
			System.err.println("PB type relation");
			return;
		}

		/* Si idée associé */
		filtres.setIdeeAssoc(ideeAssoc);

		/* Si relation entrante ou sortante */
		boolean[] relationEntrante = new boolean[arrayNbVoisinNiveau.size()];
		for (int i = 0; i < arrayNbVoisinNiveau.size(); i++) {
			relationEntrante[i] = i < arrayRelationEntrante.size() ? arrayRelationEntrante.get(i) : false;
		}
		filtres.setRelationEntrante(relationEntrante);

		// si tout les champs sont OK
		if (!ok) {
			System.err.println("pb");
			return;
		} // else

		try {
			trait.TraitementAvecFiltre(filtres);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (problemeRelationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExceptionJDM e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void setBoolean(String arg) {
		boolTerme = false;
		boolNbVoisinNiveau = false;
		boolTypeRelNiveau = false;
		boolRelationEntrante = false;
		boolIdeeAssoc = false;

		switch (arg) {
		case "-t":
			boolTerme = true;
			break;
		case "-v":
			boolNbVoisinNiveau = true;
			break;
		case "-r":
			boolTypeRelNiveau = true;
			break;
		case "-e":
			boolRelationEntrante = true;
			break;
		case "-ia":
			boolIdeeAssoc = true;
			break;
		}
	}

	private static boolean verifEtTraitRelation(String[][] relationHauteur) {
		boolean retour = true;
		ListeTypeRelation typeRel = null;
		try {
			typeRel = new ListeTypeRelation();
		} catch (problemeRelationException e1) {
			e1.printStackTrace();

			System.err.println("Problème fichier avec les listes de types de relations");
			return false;
		}

		for (int u = 0; u < relationHauteur.length; u++) {
			for (int i = 0; i < relationHauteur[u].length; i++) {
				boolean isCorrect = true;

				if (relationHauteur[u][i].equals("")) {
					relationHauteur[u][i] = "0";

				}

				if (relationHauteur[u][i].matches("-?\\d+(\\.\\d+)?")) {
					if (!typeRel.isRelation(Integer.parseInt(relationHauteur[u][i]))) {
						retour = false;
						isCorrect = false;
					} // else
				} else {
					if (!typeRel.isRelation(relationHauteur[u][i])) {
						retour = false;
						isCorrect = false;
					} else {
						relationHauteur[u][i] = "" + typeRel.getIDByRel(relationHauteur[u][i]);
					}
				}

				if (!isCorrect) {
					if (i == 0) {
						System.err.println("Numéro de relation : relation inconnue");
					} else if (i == 1) {
						System.err.println("Numéro de relation : relation inconnue");
					} else if (i == 2) {
						System.err.println("Numéro de relation : relation inconnue");
					}
				}
			}
		}
		return retour;
	}

	private static boolean verifMot(String mot) {

		RequeterRezo systeme = new RequeterRezo();
		Mot result = systeme.requete(mot);
		return (result != null);
	}

	/**
	 * Affiche l'aide
	 */
	private static void affHelp() {
		System.out.println(""
				+ "Mot (obligatoire)\n" + "\t-t terme\n\n"

				+ "Nombre de voisin par niveau (obligatoire)\n" + "\t-v n1 n2 n3 ...\n\n"

				+ "Type de relation par niveau (par defaut 0, idée associé)\n" + "\t-r n1 n2 n3\n\n"

				+ "Si relation entrante (par defaut false)\n" + "\t-e true\n" + "\t-e false\n\n"

				+ "Si idée associé (par defaut false)\n" + "\t-ia true\n" + "\t-ia false\n\n"

				+ "Exemple : \n" + "-t chat -v 10 5 1 -r 6 17 24 -e true false false -ia true\n");
	}
}
