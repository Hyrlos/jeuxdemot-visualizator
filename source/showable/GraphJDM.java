/*
 * GraphJDM.java                                6 juin 2018
 * Stagiaire LIRMM 2018
 */
package source.showable;

import java.io.FileOutputStream;
import java.io.IOException;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Element;
import org.graphstream.graph.ElementNotFoundException;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.DefaultGraph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.GraphParseException;
import org.graphstream.stream.file.FileSource;
import org.graphstream.stream.file.FileSourceFactory;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.ViewerListener;
import org.graphstream.ui.view.ViewerPipe;

/**
 * Permet de gérer la visualisation du graph basé sur rézoJDM
 * 
 * @author Thomas GEORGES
 * 
 */
public class GraphJDM implements ViewerListener {
	/** Objet venant de l'API graphStream */
	private Graph graph;
	/** Chemin vers le fichier CSS lié au graph */
	private String PATH_CSS = "style.css";
	/** Le sommet racine du graph */
	private Node racine;
	/** (id des arcs) Permet de créer des arcs uniques */
	private long idNode;
	/** pour boucle d'écoute */
	public boolean loop = true;
	/** Ecouteur d'event */
	public ViewerPipe fromViewer;
	/** Affichage */
	public Viewer viewer;
	/** Père */
	private Traitement trait;
	private boolean loading;
	private boolean affiche = true;

	GraphJDM() {
		graph = new DefaultGraph("Defaut");
	}

	/**
	 * Constructeur par defaut Appel à initialisation qui set toutes les
	 * propriétés du graphe
	 */
	public GraphJDM graphJDM(Traitement trait) {
		this.trait = trait;

		System.out.println("Lecture dernier fichier");
		this.read("last.dgs");
		initialisation();
		return this;
	}

	/**
	 * Constructeur permettant de set le titre du graphe Appel à initialisation
	 * qui set toutes les propriétés du graphe
	 * 
	 * @param titre
	 *            le titre du graphe
	 * @param trait
	 *            pointeur vers la classe traitement (appelant)
	 */
	public GraphJDM graphJDM(String titre, Traitement trait) {
		idNode = 0;
		initialisation();
		this.trait = trait;
		return this;
	}

	/**
	 * Initialisation des propriétés du graphe - Sa qualité graphique -
	 * L'utilisation de l'antiAliasing - Les bibliothèques graphiques utilisées
	 * - La permissibilité de construction - La création automatique d'une node
	 * lors de la création d'un arc
	 * 
	 */
	private void initialisation() {

		graph.addAttribute("ui.quality");
		graph.addAttribute("ui.antialias");
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		graph.addAttribute("ui.stylesheet", "url('file:" + PATH_CSS + "')");

		graph.setAutoCreate(true);
		graph.setStrict(false);

	}

	/**
	 * Affichage
	 */
	public void affichage() {
		if (affiche) {
			graph.addAttribute("ui.stylesheet", "url('file:" + PATH_CSS + "')");
			viewer = graph.display();
			viewer.enableAutoLayout();
			ThreadEcoute thread = new ThreadEcoute(this);
			thread.start();

			affiche = false;
		}
	}

	/**
	 * @return l'objet graphe
	 */
	public Graph getGraph() {
		return graph;
	}

	/**
	 * @return la racine du graphe
	 */
	public Node getRacine() {
		return racine;
	}

	/**
	 * Création d'un arc orienté nommé t n1 -> n2
	 * 
	 * @param n1
	 *            Node de départ
	 * @param t
	 *            type de relation
	 * @param n2
	 *            Node de destination
	 * @return L'objet arc ainsi crée
	 */
	public Edge addRelation(String n1, String t, String n2) {
		Edge arc = graph.addEdge("" + idNode, n1, n2, true);
		if (arc != null) {
			idNode++;
			graph.getNode(n2).addAttribute("ui.label", n2);
			graph.getNode(n1).addAttribute("ui.label", n1);
		}
		return arc;
	}

	/**
	 * Création d'un arc orienté nommé t partant de la racine rac -> n2
	 * 
	 * @param t
	 *            type de relation
	 * @param n2
	 *            Node de destination
	 * @return L'objet arc ainsi crée
	 */
	public Edge addRelationRacineToNode(String t, String n2) {
		Edge arc = graph.addEdge("" + idNode, racine.getId(), n2, true);
		idNode++;

		arc.addAttribute("ui.label", t);
		graph.getNode(n2).addAttribute("ui.label", n2);
		return arc;
	}

	/**
	 * Création d'un arc orienté nommé t ayant en destination la racine n1 ->
	 * rac
	 * 
	 * @param t
	 *            type de relation
	 * @param n1
	 *            Node de départ
	 * @return L'objet arc ainsi crée
	 */
	public Edge addRelationNodeToRacine(String t, String n1) {
		Edge arc = graph.addEdge("" + idNode, n1, racine.getId(), true);
		idNode++;

		arc.addAttribute("ui.label", t);
		graph.getNode(n1).addAttribute("ui.label", n1);
		return arc;
	}

	/**
	 * Création de la racine
	 * 
	 * @param rac
	 *            la racine
	 * @return L'objet Node associé à la racine
	 */
	public Node addRacine(String rac) {
		racine = graph.addNode(rac);
		racine.addAttribute("ui.label", rac);
		racine.addAttribute("ui.class", rac);
		return racine;
	}

	/**
	 * Créer une classe pour un arc ou une node (Utile pour le CSS)
	 * 
	 * @param arc_node
	 *            L'objet où appliquer la class
	 * @param classe
	 *            la classe à appliquer
	 */
	public static void addClass(Element arc_node, String classe) {
		arc_node.setAttribute("ui.class", classe);
	}

	/**
	 * True si node appartient au graph False sinon
	 * 
	 * @param node
	 *            la node
	 * @return si node appartient au graph
	 */
	public boolean isNode(String node) {
		return (graph.getNode(node) != null);
	}

	/**
	 * Attente d'évenement
	 * 
	 * @param lancer
	 *            s'il est nécessaire de passer en mode écoute ou non
	 */
	public void attente(boolean lancer) {
		System.out.println("Ecoute");
		/* EVENT */
		fromViewer = viewer.newViewerPipe();
		fromViewer.addViewerListener(this);
		fromViewer.addSink(graph);

		loop = true;
		while (loop && lancer) {
			fromViewer.pump();
		}
		System.out.println("Fin event");
	}

	public void viewClosed(String id) {
		if (!loading) {
			loop = false;
			this.write();
			System.out.println("save");
			trait.saveFiltres();
		}
		loading = false;
		
	}

	/** Derniere id de node activé */
	private String lastpush = "";

	/**
	 * Quand un click est detecté sur une node
	 * 
	 * @param id
	 *            id de la node venant d'etre déclenché
	 */
	public void buttonPushed(String id) {
		if (id.equals(lastpush) && !this.isNoteRacine(id)) {
			System.out.println("Double click " + id);
			try {
				trait.addRacine(id);
			} catch (ExceptionJDM e) {
				e.printStackTrace();
			}
			id = "";
		} // else
		lastpush = id;
	}

	/**
	 * Vérifie si une node est la racine
	 * 
	 * @param id
	 *            node a vérifier
	 * @return true si node est une racine
	 * 
	 */
	private boolean isNoteRacine(String id) {
		Node node = graph.getNode(id);
		return ("" + node.getAttribute("ui.class")).equals("racine");
	}

	public void buttonReleased(String id) {
		// System.out.println("Button released on node " + id);
	}

	/**
	 * TODO commenter le rôle de la méthode // TODO
	 * http://graphstream-project.org/doc/Tutorials/Reading-files-using-FileSource/
	 * 
	 */
	public void read(String path) {
		FileSource fs = null;
		try {
			fs = FileSourceFactory.sourceFor(path);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		fs.addSink(graph);

		try {
			fs.begin(path);

			while (fs.nextEvents()) {
				// Optionally some code here ...
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			fs.end();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			fs.removeSink(graph);
		}

		this.affichage();
		loading = true;
	}

	/**
	 * 
	 */
	public void write() {

		try {
			graph.write("last.dgs");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * TODO commenter le rôle de la méthode
	 * 
	 */
	public void reset() {
		graph.clear();
	}
}
