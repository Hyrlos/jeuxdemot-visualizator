/*
 * ExceptionJDM.java                                7 juin 2018
 * Stagiaire LIRMM 2018
 */
package source;

/**
 * Lancé lors des problèmes de requêtes
 * Si JDM est down ou le mot n'existe pas
 * @author Thomas GEORGES 
 *
 */

@SuppressWarnings("serial")
public class ExceptionJDM extends Exception {

	/**
	 * Lors du lancement de l'exception
	 * Affichage d'un message expliquant 
	 * que le mot n'existe pas ou que JDM est down
	 */
	public ExceptionJDM(){
		System.err.println("Mot inconnu ou connexion à JDM coupé");
	}
}
