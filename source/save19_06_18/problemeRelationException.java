/*
 * problemeRelationException.java                                5 juin 2018
 * Stagiaire LIRMM 2018
 */
package source.save19_06_18;

/**
 * Classe d'exception gérant la mauvaise gestion du fichier relationFormatee.txt
 * @author Thomas GEORGES 
 *
 */
@SuppressWarnings("serial")
public class problemeRelationException extends Exception {
	/** 
	 * Exception levé si la liste des relations ne se génère pas OU ne trouve pas le fichier
	 */
	public problemeRelationException(){
		System.out.println("liste des relations ne se génère pas OU ne trouve pas le fichier \"relationFormatee.txt\" \n");
	}
}
