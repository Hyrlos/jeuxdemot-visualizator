/*
 * InterfaceFiltres.java                                7 juin 2018
 * Stagiaire LIRMM 2018
 */
package source.save19_06_18;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.awt.event.ActionEvent;
import javax.swing.event.ChangeListener;

import requeterRezo.Mot;
import requeterRezo.RequeterRezo;

import javax.swing.event.ChangeEvent;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;

/**
 * TODO commenter la responsabilité de la classe
 * 
 * @author Thomas GEORGES
 *
 */
public class InterfaceFiltres extends JFrame {

	/** Commenter le rôle de ce champ */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtTerme;
	private JButton btnVisualiser;
	private Component horizontalStrut_7;
	private Component verticalStrut;
	private JSpinner sp_nbVoisin1;
	private JLabel lb_nbVoisin1;
	private Component verticalStrut_2;
	private Component verticalGlue;
	private Component verticalStrut_3;
	private Component verticalStrut_4;
	private JSpinner sp_nbVoisin2;
	private JSpinner sp_nbVoisin3;
	private JLabel lb_nbVoisin2;
	private JLabel lb_nbVoisin3;
	private Component verticalStrut_5;
	private Component verticalGlue_1;
	private Component verticalGlue_2;
	private Component horizontalStrut_2;
	private Component horizontalStrut_3;
	private Component horizontalStrut_4;
	private Component verticalStrut_6;
	private JLabel lblNombreDeRequete;
	private JLabel lb_nbrqtPrevu;
	private JTextField txt_relation1;
	private JLabel lblNumroDeRelation;
	private JTextField txt_relation2;
	private JLabel lblNumroDeRelation_1;
	private JTextField txt_relation3;
	private JLabel lblNumroDeRelation_2;
	private Component horizontalStrut_1;
	private Component horizontalStrut_5;
	private Component horizontalStrut_6;
	private JLabel lblNiveau_2;
	private JLabel lblNiveau_3;
	private JLabel lblNiveau1;
	private JRadioButton rdbtnRelationsEntrantes;
	private JRadioButton rdbtnRelationsSortantes;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rdbtnRelationsEntrantes_1;
	private JRadioButton rdbtnRelationsSortantes_1;
	private JRadioButton rdbtnRelationsEntrantes_2;
	private JRadioButton rdbtnRelationsSortantes_2;
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();
	private JCheckBox chckbxAvecIdesAssocies;

	/**
	 * Create the frame.
	 */
	public InterfaceFiltres(Filtres filtres) {
		setTitle("JDM_VISUALISATION");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 621, 772);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0 };
		gbl_panel.columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JLabel lb_titre = new JLabel("Filtres du graphe");
		lb_titre.setFont(new Font("Arial", Font.BOLD, 20));
		GridBagConstraints gbc_lb_titre = new GridBagConstraints();
		gbc_lb_titre.gridwidth = 11;
		gbc_lb_titre.insets = new Insets(0, 0, 5, 5);
		gbc_lb_titre.gridx = 1;
		gbc_lb_titre.gridy = 1;
		panel.add(lb_titre, gbc_lb_titre);

		JLabel lblMot = new JLabel("Terme");
		lblMot.setFont(new Font("Arial", Font.PLAIN, 15));
		GridBagConstraints gbc_lblMot = new GridBagConstraints();
		gbc_lblMot.gridwidth = 2;
		gbc_lblMot.fill = GridBagConstraints.VERTICAL;
		gbc_lblMot.anchor = GridBagConstraints.WEST;
		gbc_lblMot.insets = new Insets(0, 0, 5, 5);
		gbc_lblMot.gridx = 1;
		gbc_lblMot.gridy = 4;
		panel.add(lblMot, gbc_lblMot);

		txtTerme = new JTextField();
		GridBagConstraints gbc_txtMot = new GridBagConstraints();
		gbc_txtMot.gridwidth = 11;
		gbc_txtMot.insets = new Insets(0, 0, 5, 5);
		gbc_txtMot.fill = GridBagConstraints.BOTH;
		gbc_txtMot.gridx = 1;
		gbc_txtMot.gridy = 5;
		panel.add(txtTerme, gbc_txtMot);
		txtTerme.setColumns(10);

		chckbxAvecIdesAssocies = new JCheckBox("Avec idées associées");
		GridBagConstraints gbc_chckbxAvecIdesAssocies = new GridBagConstraints();
		gbc_chckbxAvecIdesAssocies.anchor = GridBagConstraints.WEST;
		gbc_chckbxAvecIdesAssocies.gridwidth = 5;
		gbc_chckbxAvecIdesAssocies.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxAvecIdesAssocies.gridx = 1;
		gbc_chckbxAvecIdesAssocies.gridy = 7;
		panel.add(chckbxAvecIdesAssocies, gbc_chckbxAvecIdesAssocies);

		lblNiveau1 = new JLabel("Niveau 1");
		GridBagConstraints gbc_lblNiveau1 = new GridBagConstraints();
		gbc_lblNiveau1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNiveau1.gridx = 1;
		gbc_lblNiveau1.gridy = 9;
		panel.add(lblNiveau1, gbc_lblNiveau1);

		lb_nbVoisin1 = new JLabel("Nombre de voisins");
		lb_nbVoisin1.setFont(new Font("Arial", Font.PLAIN, 15));
		GridBagConstraints gbc_lb_nbVoisin1 = new GridBagConstraints();
		gbc_lb_nbVoisin1.anchor = GridBagConstraints.NORTHWEST;
		gbc_lb_nbVoisin1.gridwidth = 2;
		gbc_lb_nbVoisin1.insets = new Insets(0, 0, 5, 5);
		gbc_lb_nbVoisin1.gridx = 1;
		gbc_lb_nbVoisin1.gridy = 10;
		panel.add(lb_nbVoisin1, gbc_lb_nbVoisin1);

		lblNumroDeRelation = new JLabel("Numéro de relation");
		GridBagConstraints gbc_lblNumroDeRelation = new GridBagConstraints();
		gbc_lblNumroDeRelation.gridwidth = 3;
		gbc_lblNumroDeRelation.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblNumroDeRelation.insets = new Insets(0, 0, 5, 5);
		gbc_lblNumroDeRelation.gridx = 3;
		gbc_lblNumroDeRelation.gridy = 10;
		panel.add(lblNumroDeRelation, gbc_lblNumroDeRelation);

		sp_nbVoisin1 = new JSpinner();
		sp_nbVoisin1.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				lb_nbrqtPrevu.setText("" + ((int) sp_nbVoisin1.getValue()
						* (1 + (int) sp_nbVoisin2.getValue() * (1 + (int) sp_nbVoisin3.getValue()))));

				// int ok = (int) sp_nbVoisin1.getValue();
			}
		});
		sp_nbVoisin1.setModel(new SpinnerNumberModel(new Integer(5), new Integer(0), null, new Integer(1)));
		GridBagConstraints gbc_sp_nbVoisin1 = new GridBagConstraints();
		gbc_sp_nbVoisin1.anchor = GridBagConstraints.NORTH;
		gbc_sp_nbVoisin1.fill = GridBagConstraints.HORIZONTAL;
		gbc_sp_nbVoisin1.insets = new Insets(0, 0, 5, 5);
		gbc_sp_nbVoisin1.gridx = 1;
		gbc_sp_nbVoisin1.gridy = 12;
		panel.add(sp_nbVoisin1, gbc_sp_nbVoisin1);

		txt_relation1 = new JTextField();
		txt_relation1.setText("6");
		GridBagConstraints gbc_txt_relation1 = new GridBagConstraints();
		gbc_txt_relation1.gridwidth = 4;
		gbc_txt_relation1.insets = new Insets(0, 0, 5, 5);
		gbc_txt_relation1.fill = GridBagConstraints.HORIZONTAL;
		gbc_txt_relation1.gridx = 3;
		gbc_txt_relation1.gridy = 12;
		panel.add(txt_relation1, gbc_txt_relation1);
		txt_relation1.setColumns(10);

		rdbtnRelationsEntrantes = new JRadioButton("relations entrantes");
		buttonGroup.add(rdbtnRelationsEntrantes);
		rdbtnRelationsEntrantes.setMnemonic('a');
		GridBagConstraints gbc_rdbtnRelationsEntrantes = new GridBagConstraints();
		gbc_rdbtnRelationsEntrantes.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnRelationsEntrantes.gridx = 8;
		gbc_rdbtnRelationsEntrantes.gridy = 12;
		panel.add(rdbtnRelationsEntrantes, gbc_rdbtnRelationsEntrantes);

		rdbtnRelationsSortantes = new JRadioButton("relations sortantes");
		rdbtnRelationsSortantes.setSelected(true);
		buttonGroup.add(rdbtnRelationsSortantes);
		rdbtnRelationsSortantes.setMnemonic('a');
		GridBagConstraints gbc_rdbtnRelationsSortantes = new GridBagConstraints();
		gbc_rdbtnRelationsSortantes.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnRelationsSortantes.gridx = 8;
		gbc_rdbtnRelationsSortantes.gridy = 13;
		panel.add(rdbtnRelationsSortantes, gbc_rdbtnRelationsSortantes);

		lblNiveau_2 = new JLabel("Niveau 2");
		GridBagConstraints gbc_lblNiveau_2 = new GridBagConstraints();
		gbc_lblNiveau_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNiveau_2.gridx = 1;
		gbc_lblNiveau_2.gridy = 16;
		panel.add(lblNiveau_2, gbc_lblNiveau_2);

		lb_nbVoisin2 = new JLabel("Nombre de voisins");
		lb_nbVoisin2.setFont(new Font("Arial", Font.PLAIN, 15));
		GridBagConstraints gbc_lb_nbVoisin2 = new GridBagConstraints();
		gbc_lb_nbVoisin2.gridwidth = 2;
		gbc_lb_nbVoisin2.anchor = GridBagConstraints.NORTHWEST;
		gbc_lb_nbVoisin2.insets = new Insets(0, 0, 5, 5);
		gbc_lb_nbVoisin2.gridx = 1;
		gbc_lb_nbVoisin2.gridy = 17;
		panel.add(lb_nbVoisin2, gbc_lb_nbVoisin2);

		sp_nbVoisin2 = new JSpinner();
		sp_nbVoisin2.setModel(new SpinnerNumberModel(new Integer(3), new Integer(0), null, new Integer(1)));
		sp_nbVoisin2.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				lb_nbrqtPrevu.setText("" + ((int) sp_nbVoisin1.getValue()
						* (1 + (int) sp_nbVoisin2.getValue() * (1 + (int) sp_nbVoisin3.getValue()))));
			}
		});

		lblNumroDeRelation_1 = new JLabel("Numéro de relation");
		GridBagConstraints gbc_lblNumroDeRelation_1 = new GridBagConstraints();
		gbc_lblNumroDeRelation_1.gridwidth = 3;
		gbc_lblNumroDeRelation_1.anchor = GridBagConstraints.WEST;
		gbc_lblNumroDeRelation_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNumroDeRelation_1.gridx = 3;
		gbc_lblNumroDeRelation_1.gridy = 17;
		panel.add(lblNumroDeRelation_1, gbc_lblNumroDeRelation_1);
		GridBagConstraints gbc_sp_nbVoisin2 = new GridBagConstraints();
		gbc_sp_nbVoisin2.fill = GridBagConstraints.HORIZONTAL;
		gbc_sp_nbVoisin2.anchor = GridBagConstraints.NORTH;
		gbc_sp_nbVoisin2.insets = new Insets(0, 0, 5, 5);
		gbc_sp_nbVoisin2.gridx = 1;
		gbc_sp_nbVoisin2.gridy = 18;
		panel.add(sp_nbVoisin2, gbc_sp_nbVoisin2);

		txt_relation2 = new JTextField();
		txt_relation2.setText("17");
		GridBagConstraints gbc_txt_relation2 = new GridBagConstraints();
		gbc_txt_relation2.anchor = GridBagConstraints.NORTH;
		gbc_txt_relation2.gridwidth = 4;
		gbc_txt_relation2.insets = new Insets(0, 0, 5, 5);
		gbc_txt_relation2.fill = GridBagConstraints.HORIZONTAL;
		gbc_txt_relation2.gridx = 3;
		gbc_txt_relation2.gridy = 18;
		panel.add(txt_relation2, gbc_txt_relation2);
		txt_relation2.setColumns(10);

		rdbtnRelationsEntrantes_1 = new JRadioButton("relations entrantes");
		buttonGroup_1.add(rdbtnRelationsEntrantes_1);
		GridBagConstraints gbc_rdbtnRelationsEntrantes_1 = new GridBagConstraints();
		gbc_rdbtnRelationsEntrantes_1.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnRelationsEntrantes_1.gridx = 8;
		gbc_rdbtnRelationsEntrantes_1.gridy = 18;
		panel.add(rdbtnRelationsEntrantes_1, gbc_rdbtnRelationsEntrantes_1);

		rdbtnRelationsSortantes_1 = new JRadioButton("relations sortantes");
		rdbtnRelationsSortantes_1.setSelected(true);
		buttonGroup_1.add(rdbtnRelationsSortantes_1);
		GridBagConstraints gbc_rdbtnRelationsSortantes_1 = new GridBagConstraints();
		gbc_rdbtnRelationsSortantes_1.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnRelationsSortantes_1.gridx = 8;
		gbc_rdbtnRelationsSortantes_1.gridy = 19;
		panel.add(rdbtnRelationsSortantes_1, gbc_rdbtnRelationsSortantes_1);

		lblNiveau_3 = new JLabel("Niveau 3");
		GridBagConstraints gbc_lblNiveau_3 = new GridBagConstraints();
		gbc_lblNiveau_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNiveau_3.gridx = 1;
		gbc_lblNiveau_3.gridy = 20;
		panel.add(lblNiveau_3, gbc_lblNiveau_3);

		lb_nbVoisin3 = new JLabel("Nombre de voisins");
		lb_nbVoisin3.setFont(new Font("Arial", Font.PLAIN, 15));
		GridBagConstraints gbc_lb_nbVoisin3 = new GridBagConstraints();
		gbc_lb_nbVoisin3.gridwidth = 2;
		gbc_lb_nbVoisin3.anchor = GridBagConstraints.NORTHWEST;
		gbc_lb_nbVoisin3.insets = new Insets(0, 0, 5, 5);
		gbc_lb_nbVoisin3.gridx = 1;
		gbc_lb_nbVoisin3.gridy = 22;
		panel.add(lb_nbVoisin3, gbc_lb_nbVoisin3);

		sp_nbVoisin3 = new JSpinner();
		sp_nbVoisin3.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		sp_nbVoisin3.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				lb_nbrqtPrevu.setText("" + ((int) sp_nbVoisin1.getValue()
						* (1 + (int) sp_nbVoisin2.getValue() * (1 + (int) sp_nbVoisin3.getValue()))));
			}
		});

		lblNumroDeRelation_2 = new JLabel("Numéro de relation");
		GridBagConstraints gbc_lblNumroDeRelation_2 = new GridBagConstraints();
		gbc_lblNumroDeRelation_2.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblNumroDeRelation_2.gridwidth = 3;
		gbc_lblNumroDeRelation_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNumroDeRelation_2.gridx = 3;
		gbc_lblNumroDeRelation_2.gridy = 22;
		panel.add(lblNumroDeRelation_2, gbc_lblNumroDeRelation_2);
		GridBagConstraints gbc_sp_nbVoisin3 = new GridBagConstraints();
		gbc_sp_nbVoisin3.anchor = GridBagConstraints.NORTH;
		gbc_sp_nbVoisin3.fill = GridBagConstraints.HORIZONTAL;
		gbc_sp_nbVoisin3.insets = new Insets(0, 0, 5, 5);
		gbc_sp_nbVoisin3.gridx = 1;
		gbc_sp_nbVoisin3.gridy = 23;
		panel.add(sp_nbVoisin3, gbc_sp_nbVoisin3);

		/* Lance la visualisation du graphe */
		JFrame me = this;
		btnVisualiser = new JButton("Visualiser");
		btnVisualiser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean ok = true;

				/* TERME */
				String mot = txtTerme.getText();
				if (verifMot(mot)) {
					filtres.setTerme(txtTerme.getText());
					lblMot.setText("Terme");
				} else {
					lblMot.setText("Terme (Mot incorrect)");
					ok = false;
				}

				/* Relation hauteur */
				String[] relationHauteur = { txt_relation1.getText(), txt_relation2.getText(),
						txt_relation3.getText() };
				if (verifEtTraitRelation(relationHauteur)) {
					filtres.setRelationHauteur(relationHauteur);
				} else {
					ok = false;
				}

				/* Nombre voisins */
				int[] nbVoisin = { (int) sp_nbVoisin1.getValue(), (int) sp_nbVoisin2.getValue(),
						(int) sp_nbVoisin3.getValue() };
				filtres.setNbVoisin(nbVoisin);

				/* Si idée associé */
				filtres.setIdeeAssoc(chckbxAvecIdesAssocies.isSelected());

				/* Si relation entrante ou sortante */
				boolean[] relationEntrante = { rdbtnRelationsEntrantes.isSelected(),
						rdbtnRelationsEntrantes_1.isSelected(), rdbtnRelationsEntrantes_2.isSelected() };
				filtres.setRelationEntrante(relationEntrante);

				// si tout les champs sont OK
				if (!ok) {
					return;
				} // else

				me.dispose();

				try {
					new Traitement(filtres);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (problemeRelationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExceptionJDM e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			private boolean verifEtTraitRelation(String[] relationHauteur) {
				boolean retour = true;
				ListeTypeRelation typeRel = null;
				try {
					typeRel = new ListeTypeRelation();
				} catch (problemeRelationException e1) {
					e1.printStackTrace();

					lb_titre.setText("Problème fichier avec les listes de types de relations");
					return false;
				}

				for (int i = 0; i < relationHauteur.length; i++) {
					boolean isCorrect = true;

					if (relationHauteur[i].equals("")) {
						relationHauteur[i] = "0";

					}

					if (relationHauteur[i].matches("-?\\d+(\\.\\d+)?")) {
						if (!typeRel.isRelation(Integer.parseInt(relationHauteur[i]))) {
							retour = false;
							isCorrect = false;
						} // else
					} else {
						if (!typeRel.isRelation(relationHauteur[i])) {
							retour = false;
							isCorrect = false;
						} // else
					}

					if (!isCorrect) {
						if (i == 0) {
							lblNumroDeRelation.setText("Numéro de relation : relation inconnue");
						} else if (i == 1) {
							lblNumroDeRelation_1.setText("Numéro de relation : relation inconnue");
						} else if (i == 2) {
							lblNumroDeRelation_2.setText("Numéro de relation : relation inconnue");
						}
					}
				}

				if (retour) {
					lblNumroDeRelation.setText("Numéro de relation");
					lblNumroDeRelation_1.setText("Numéro de relation");
					lblNumroDeRelation_2.setText("Numéro de relation");

				}
				return retour;
			}

			private boolean verifMot(String mot) {

				RequeterRezo systeme = new RequeterRezo("36h", 3000);
				Mot result = systeme.requete(mot);
				return (result != null);
			}
		});

		txt_relation3 = new JTextField();
		GridBagConstraints gbc_txt_relation3 = new GridBagConstraints();
		gbc_txt_relation3.gridwidth = 4;
		gbc_txt_relation3.anchor = GridBagConstraints.NORTH;
		gbc_txt_relation3.insets = new Insets(0, 0, 5, 5);
		gbc_txt_relation3.fill = GridBagConstraints.HORIZONTAL;
		gbc_txt_relation3.gridx = 3;
		gbc_txt_relation3.gridy = 23;
		panel.add(txt_relation3, gbc_txt_relation3);
		txt_relation3.setColumns(10);

		rdbtnRelationsEntrantes_2 = new JRadioButton("relations entrantes");
		buttonGroup_2.add(rdbtnRelationsEntrantes_2);
		GridBagConstraints gbc_rdbtnRelationsEntrantes_2 = new GridBagConstraints();
		gbc_rdbtnRelationsEntrantes_2.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnRelationsEntrantes_2.gridx = 8;
		gbc_rdbtnRelationsEntrantes_2.gridy = 23;
		panel.add(rdbtnRelationsEntrantes_2, gbc_rdbtnRelationsEntrantes_2);

		rdbtnRelationsSortantes_2 = new JRadioButton("relation sortantes");
		rdbtnRelationsSortantes_2.setSelected(true);
		buttonGroup_2.add(rdbtnRelationsSortantes_2);
		GridBagConstraints gbc_rdbtnRelationsSortantes_2 = new GridBagConstraints();
		gbc_rdbtnRelationsSortantes_2.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnRelationsSortantes_2.gridx = 8;
		gbc_rdbtnRelationsSortantes_2.gridy = 24;
		panel.add(rdbtnRelationsSortantes_2, gbc_rdbtnRelationsSortantes_2);

		lblNombreDeRequete = new JLabel(
				"Nombre de requetes : (Un grand nombre de requete rendra le graph moins lisible et augmentera le temps de traitement");
		lblNombreDeRequete.setHorizontalAlignment(SwingConstants.TRAILING);
		GridBagConstraints gbc_lblNombreDeRequete = new GridBagConstraints();
		gbc_lblNombreDeRequete.anchor = GridBagConstraints.WEST;
		gbc_lblNombreDeRequete.gridheight = 2;
		gbc_lblNombreDeRequete.gridwidth = 11;
		gbc_lblNombreDeRequete.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombreDeRequete.gridx = 1;
		gbc_lblNombreDeRequete.gridy = 25;
		panel.add(lblNombreDeRequete, gbc_lblNombreDeRequete);

		lb_nbrqtPrevu = new JLabel("30");
		GridBagConstraints gbc_lb_nbrqtPrevu = new GridBagConstraints();
		gbc_lb_nbrqtPrevu.anchor = GridBagConstraints.WEST;
		gbc_lb_nbrqtPrevu.insets = new Insets(0, 0, 5, 5);
		gbc_lb_nbrqtPrevu.gridx = 1;
		gbc_lb_nbrqtPrevu.gridy = 27;
		panel.add(lb_nbrqtPrevu, gbc_lb_nbrqtPrevu);
		btnVisualiser.setFont(new Font("Arial", Font.PLAIN, 15));
		GridBagConstraints gbc_btnVisualiser = new GridBagConstraints();
		gbc_btnVisualiser.anchor = GridBagConstraints.NORTHWEST;
		gbc_btnVisualiser.insets = new Insets(0, 0, 5, 5);
		gbc_btnVisualiser.gridx = 1;
		gbc_btnVisualiser.gridy = 28;
		panel.add(btnVisualiser, gbc_btnVisualiser);

		/* ================= Strut et Glue ================== */

		verticalStrut = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut.gridx = 1;
		gbc_verticalStrut.gridy = 24;
		panel.add(verticalStrut, gbc_verticalStrut);

		horizontalStrut_4 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_4 = new GridBagConstraints();
		gbc_horizontalStrut_4.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_4.gridx = 2;
		gbc_horizontalStrut_4.gridy = 23;
		panel.add(horizontalStrut_4, gbc_horizontalStrut_4);

		horizontalStrut_3 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_3 = new GridBagConstraints();
		gbc_horizontalStrut_3.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_3.gridx = 2;
		gbc_horizontalStrut_3.gridy = 18;
		panel.add(horizontalStrut_3, gbc_horizontalStrut_3);

		verticalGlue_1 = Box.createVerticalGlue();
		GridBagConstraints gbc_verticalGlue_1 = new GridBagConstraints();
		gbc_verticalGlue_1.insets = new Insets(0, 0, 5, 5);
		gbc_verticalGlue_1.gridx = 1;
		gbc_verticalGlue_1.gridy = 21;
		panel.add(verticalGlue_1, gbc_verticalGlue_1);

		horizontalStrut_2 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_2 = new GridBagConstraints();
		gbc_horizontalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_2.gridx = 2;
		gbc_horizontalStrut_2.gridy = 12;
		panel.add(horizontalStrut_2, gbc_horizontalStrut_2);

		verticalStrut_5 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_5 = new GridBagConstraints();
		gbc_verticalStrut_5.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut_5.gridx = 1;
		gbc_verticalStrut_5.gridy = 14;
		panel.add(verticalStrut_5, gbc_verticalStrut_5);

		verticalGlue_2 = Box.createVerticalGlue();
		GridBagConstraints gbc_verticalGlue_2 = new GridBagConstraints();
		gbc_verticalGlue_2.insets = new Insets(0, 0, 5, 5);
		gbc_verticalGlue_2.gridx = 1;
		gbc_verticalGlue_2.gridy = 15;
		panel.add(verticalGlue_2, gbc_verticalGlue_2);

		verticalStrut_2 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_2 = new GridBagConstraints();
		gbc_verticalStrut_2.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut_2.gridx = 1;
		gbc_verticalStrut_2.gridy = 8;
		panel.add(verticalStrut_2, gbc_verticalStrut_2);

		horizontalStrut_7 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_7 = new GridBagConstraints();
		gbc_horizontalStrut_7.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalStrut_7.gridx = 12;
		gbc_horizontalStrut_7.gridy = 5;
		panel.add(horizontalStrut_7, gbc_horizontalStrut_7);

		verticalStrut_3 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_3 = new GridBagConstraints();
		gbc_verticalStrut_3.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut_3.gridx = 1;
		gbc_verticalStrut_3.gridy = 2;
		panel.add(verticalStrut_3, gbc_verticalStrut_3);

		verticalStrut_4 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_4 = new GridBagConstraints();
		gbc_verticalStrut_4.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut_4.gridx = 1;
		gbc_verticalStrut_4.gridy = 3;
		panel.add(verticalStrut_4, gbc_verticalStrut_4);

		verticalStrut_6 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_6 = new GridBagConstraints();
		gbc_verticalStrut_6.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut_6.gridx = 2;
		gbc_verticalStrut_6.gridy = 0;
		panel.add(verticalStrut_6, gbc_verticalStrut_6);

		horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 7;
		gbc_horizontalStrut_1.gridy = 12;
		panel.add(horizontalStrut_1, gbc_horizontalStrut_1);

		horizontalStrut_5 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_5 = new GridBagConstraints();
		gbc_horizontalStrut_5.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_5.gridx = 7;
		gbc_horizontalStrut_5.gridy = 18;
		panel.add(horizontalStrut_5, gbc_horizontalStrut_5);

		verticalGlue = Box.createVerticalGlue();
		GridBagConstraints gbc_verticalGlue = new GridBagConstraints();
		gbc_verticalGlue.insets = new Insets(0, 0, 5, 5);
		gbc_verticalGlue.gridx = 1;
		gbc_verticalGlue.gridy = 11;
		panel.add(verticalGlue, gbc_verticalGlue);

		horizontalStrut_6 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_6 = new GridBagConstraints();
		gbc_horizontalStrut_6.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_6.gridx = 7;
		gbc_horizontalStrut_6.gridy = 23;
		panel.add(horizontalStrut_6, gbc_horizontalStrut_6);
	}

}
