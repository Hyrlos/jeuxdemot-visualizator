/*
 * Filtres.java                                7 juin 2018
 * Stagiaire LIRMM 2018
 */
package source.save19_06_18;

/**
 * Objet implémentant les différents filtres 
 * utilisable par la visualisation du graphe de rezoJDM
 * @author Thomas GEORGES 
 *
 */
public class Filtres {
	/** Mot racine du graphe */
	private String terme;
	/** Nombre de voisin par niveau */
	private int[] nbVoisin;
	/** relationHauteur[relation] : indice = hauteur */
	private String[] relationHauteur;
	/** Nombre de nombres de voisins */
	private int nb_nbVoisin;
	/** Si les relation sont entrante ou sortante */
	private boolean[] relationEntrante;
	/** Si les relation sont entrante ou sortante */
	private boolean ideeAssoc;

	/**
	 * Constructeur par défaut, initialise les champs
	 */
	public Filtres(){
		this.terme = "";
		int[] tab = {};
		String [] tab1 = {};
		boolean [] tab2 = {};
		this.relationHauteur = tab1;
		this.nbVoisin = tab;		
		this.relationEntrante = tab2;
	}

	/**
	 * @return terme
	 */
	public String getTerme() {
		return terme;
	}

	/**
	 * @param terme nouvelle valeur de terme
	 */
	public void setTerme(String terme) {
		this.terme = terme;
	}

	/**
	 * @return nbVoisin
	 */
	public int[] getNbVoisin() {
		return nbVoisin;
	}

	/**
	 * @param nbVoisin nouvelle valeur de nbVoisin
	 */
	public void setNbVoisin(int[] nbVoisin) {
		this.nbVoisin = nbVoisin;
		this.nb_nbVoisin = calculNb_nbVoisin();
	}

	/**
	 * @param nbVoisin nouvelle valeur de nbVoisin
	 * @param indice à l'indice indice
	 */
	public void setNbVoisinIndice(int nbVoisin, int indice) {
		this.nbVoisin[indice] = nbVoisin;
		this.nb_nbVoisin = calculNb_nbVoisin();
	}
	
	/**
	 * @param indice l'indice du tableau
	 * @return le nombre de voisin de hauteur indice
	 */
	public int getNbVoisinIndice(int indice) {
		return nbVoisin[indice];
	}
	
	/**
	 * @param indice l'indice du tableau
	 * @return la relation des voisins à l'hauteur indice
	 */
	public String getRelation(int indice) {
		return relationHauteur[indice];
	}

	/**
	 * @param indice l'indice du tableau
	 * @param relation nouvelle valeur de relation
	 */
	public void setRelation(String relation, int indice) {
		this.relationHauteur[indice] = relation;
	}

	
	
	/**
	 * @return relationHauteur
	 */
	public String[] getRelationHauteur() {
		return relationHauteur;
	}

	/**
	 * @param relationHauteur nouvelle valeur de relationHauteur
	 */
	public void setRelationHauteur(String[] relationHauteur) {
		this.relationHauteur = relationHauteur;
	}

	/**
	 * @return nb_nbVoisin
	 */
	public int calculNb_nbVoisin(){
		for(int i = 0; i < nbVoisin.length; i++){
			if(nbVoisin[i] == 0){
				return i;
			}
		}
		return nbVoisin.length;
	}
	
	/**
	 * @return nb_nbVoisin
	 */
	public int getNb_nbVoisin(){
		return nb_nbVoisin;
	}

	/**
	 * @return relationEntrante
	 */
	public boolean isRelationEntrante(int indice) {
		return relationEntrante[indice];
	}

	/**
	 * @param relationEntrante nouvelle valeur de relationEntrante
	 */
	public void setRelationEntrante(boolean[] relationEntrante) {
		this.relationEntrante = relationEntrante;
	}

	/**
	 * @return ideeAssoc
	 */
	public boolean isIdeeAssoc() {
		return ideeAssoc;
	}

	/**
	 * @param ideeAssoc nouvelle valeur de ideeAssoc
	 */
	public void setIdeeAssoc(boolean ideeAssoc) {
		this.ideeAssoc = ideeAssoc;
	}
	
	
}
