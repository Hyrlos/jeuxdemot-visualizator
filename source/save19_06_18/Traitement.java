/*
 * Traitement.java                                7 juin 2018
 * Stagiaire LIRMM 2018
 */
package source.save19_06_18;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;

import requeterRezo.Filtre;
import requeterRezo.Mot;
import requeterRezo.RequeterRezo;
import requeterRezo.Voisin;

/**
 * Traitement et mise en relation des données Puis affichage et gestion du
 * graphe
 * 
 * @author Thomas GEORGES
 *
 */
public class Traitement {
	/** Objet manipulant les filtres */
	private Filtres filtres;
	/** Objet contenant la structure du graphe de JDM */
	private GraphJDM graph;
	/** Requêtteur de rezoJDM */
	private RequeterRezo systeme;
	/** Structure avec la liste des types de relations de rezoJDM */
	private ListeTypeRelation listeRelation;
	/** tableau contenant les relations à afficher format (n1, t, n2, w) */
	private ArrayList<String[]> touteRelationAffi;
	/** tableau contenant les relations format (n1, t, n2, w) */
	private ArrayList<String[]> touteRelation;
	/** tableau contenant les raffinement format (n1, t, n2, w) */
	private ArrayList<String[]> raffRelation;
	/** tableau contenant les idées associées format (n1, t, n2, w) */
	private ArrayList<String[]> ideeAssocRelation;
	/** tableau contenant les node format (n1, w) */
	private ArrayList<String[]> node;

	/**
	 * Indice permettant de savoir où l'on s'es arreté dans l'affichage du
	 * tableau relation
	 */
	private int indiceCourantRelation;

	public Traitement(Filtres filtres) throws MalformedURLException, problemeRelationException, ExceptionJDM, IOException, InterruptedException{
		this.filtres = filtres;
		
		
		TraitementFiltre();
		
		System.out.println("Début boucle");
		graph.attente(false);
		System.out.println("Fin");
	}
	
	/**
	 * traitement et mise en relation des données et du graphe
	 * 
	 * @param filtres
	 *            la structure contenant les filtres
	 * @return 
	 * @throws problemeRelationException
	 *             Si la liste des types de relations est mal généré
	 * @throws ExceptionJDM
	 *             Si problème sur le requêtteur
	 * @throws MalformedURLException
	 *             Problème d'URL
	 * @throws IOException
	 *             Problème d'affichage
	 * @throws InterruptedException
	 *             Problème ?
	 */
	public void TraitementFiltre()
			throws problemeRelationException, ExceptionJDM, MalformedURLException, IOException, InterruptedException {

		System.out.println("Traitement : ");

		/* Initialisations */
		touteRelationAffi = new ArrayList<String[]>();
		touteRelation = new ArrayList<String[]>();
		ideeAssocRelation = new ArrayList<String[]>();
		raffRelation = new ArrayList<String[]>();
		node = new ArrayList<String[]>();

		
		listeRelation = new ListeTypeRelation();
		graph = new GraphJDM(filtres.getTerme());
		indiceCourantRelation = 0;

		systeme = new RequeterRezo("36h", 3000);

		/* Affichage des filtres */
		System.out.println("\n\tFiltres :");
		System.out.println("Mot : " + filtres.getTerme());
		System.out.println("Relations " + (filtres.isRelationEntrante(0) ? "sortante" : "entrante"));
		String[] tab1 = filtres.getRelationHauteur();
		int[] tab = filtres.getNbVoisin();
		for (int i = 0; i < filtres.getNb_nbVoisin(); i++) {
			System.out.println("Voisin de distance " + (i + 1) + " : " + tab[i] + " avec la relation "
					+ listeRelation.getRelById(Integer.parseInt(tab1[i])));
		}


		System.out.println("\n Affichage graph");
		graph.affichage();
		
		System.out.println("\n\tRelation :");
		/* Rempli le tableau des relations */
		remplirRelation();

		System.out.println("\n Insertion graphe");
		affichage();

		/* ajoute au tableau des relations les relations de raffinement utile */
		System.out.println("\n\t Rafinement");
		insertionRaff_ideeAssoc();

		System.out.println("\n Insertion graphe");
		affichage();

		System.out.println("\n\t Attente event");

		/* Pour JDM */
		systeme.sauvegarder();
		
		
	}

	/**
	 * Remplissage du tableau relation à afficher à partir d'un mot et de sa
	 * hauteur
	 * 
	 * @param mot
	 *            mot racine
	 * @param nb
	 *            nombre de relation à ajouter au tableau
	 * @param hauteur
	 *            le niveau du graphe racine = 0 relation 1er ordre = 1 ...
	 * @throws ExceptionJDM
	 */
	private void remplirRelation() throws ExceptionJDM {

		ArrayList<String[]> relationCourante = new ArrayList<String[]>();
		ArrayList<Voisin> listeRelationType0 = new ArrayList<Voisin>();
		ArrayList<Voisin> listeRelationType1 = new ArrayList<Voisin>();
		ArrayList<Voisin> listeRelationTypeChoix = new ArrayList<Voisin>();
		HashMap<String, ArrayList<Voisin>> listeEntree = new HashMap<String, ArrayList<Voisin>>();

		String[] nodeCourante = { filtres.getTerme(), "0" };
		node.add(nodeCourante);

		/* Pour toutes les nodes */
		for (int i = 0; i < node.size(); i++) {
			ArrayList<String[]> relations = new ArrayList<String[]>();
			int hauteur = Integer.parseInt(node.get(i)[1]);
			if (hauteur < filtres.getNb_nbVoisin()) {
				String mot = node.get(i)[0];
				String idRelation = listeRelation.getRelById(Integer.parseInt(filtres.getRelation(hauteur)));
				boolean isRelationEntrante = filtres.isRelationEntrante(hauteur);

				System.out.println(mot + " " + hauteur + " " + idRelation);

				Mot current = null;
				try {
					current = systeme.requete(mot);
				} catch (Exception e) {
					System.err.println(e + "\n erreur mot");
				}

				if (i == 0) {
					if (current == null) {
						// TODO pb requete initial
					}
					Node rac = graph.addRacine(filtres.getTerme());
					GraphJDM.addClass(rac, "racine");
				}

				if (current != null) {

					/* On prends les relations entrantes OU sortantes */
					if (isRelationEntrante) {
						listeEntree = current.getRelations_entrantes();
					} else {
						listeEntree = current.getRelations_sortantes();
					}

					/* Pour toutes les relations */
					// Relation courante u
					listeRelationType0 = listeEntree.get(listeRelation.getRelById(0));
					listeRelationType1 = listeEntree.get(listeRelation.getRelById(1));
					listeRelationTypeChoix = listeEntree.get(idRelation);

					// si la relation est sementique
					if (listeRelationType1 != null) {
						for (int v = 0; v < listeRelationType1.size(); v++) {
							String n2 = isRelationEntrante ? mot : listeRelationType1.get(v).getNom();
							String n1 = isRelationEntrante ? listeRelationType1.get(v).getNom() : mot;
							String[] tempo = { n1, listeRelation.getRelById(1), n2,
									"" + listeRelationType1.get(v).getPoids() };
							raffRelation.add(tempo);
						}
					}

					// si la relation est idée associée
					if (listeRelationType0 != null) {
						for (int v = 0; v < listeRelationType0.size(); v++) {
							String n2 = isRelationEntrante ? mot : listeRelationType0.get(v).getNom();
							String n1 = isRelationEntrante ? listeRelationType0.get(v).getNom() : mot;
							String[] tempo = { n1, listeRelation.getRelById(0), n2,
									"" + listeRelationType0.get(v).getPoids() };
							ideeAssocRelation.add(tempo);
						}
					}
					// si la relation est celle recherché
					if (listeRelationTypeChoix != null) {
						// Pour chaque mot avec cette relation
						for (int v = 0; v < listeRelationTypeChoix.size(); v++) {
							String n2 = isRelationEntrante ? mot : listeRelationTypeChoix.get(v).getNom();
							String n1 = isRelationEntrante ? listeRelationTypeChoix.get(v).getNom() : mot;
							String[] tempo = { n1, idRelation, n2, "" + listeRelationTypeChoix.get(v).getPoids() };

							relations.add(tempo);
						}

						TrieEtAjoutList(relations, filtres.getNbVoisinIndice(hauteur), hauteur);
					}
				}
			}
		}
	}

	private void TrieEtAjoutList(ArrayList<String[]> aTrier, int nb, int hauteur) {
		for (int u = 0; u < aTrier.size() && u < nb && aTrier.size() > 0; u++) {
			int indice = 0;

			for (int i = 0; i < aTrier.size(); i++) {
				
				if (Math.abs(Double.parseDouble(aTrier.get(i)[3])) > Math
						.abs(Double.parseDouble(aTrier.get(indice)[3])) 
						&& aTrier.get(i)[0].charAt(0)!='_' &&  aTrier.get(i)[2].charAt(0)!='_') {
					indice = i;
				}
			}

			touteRelationAffi.add(aTrier.get(indice));
			System.out.println(aTrier.get(indice)[0] + " " + aTrier.get(indice)[1] + " " + aTrier.get(indice)[2] + " "
					+ aTrier.get(indice)[3]);
			String mot = filtres.isRelationEntrante(hauteur) ? aTrier.get(indice)[0] : aTrier.get(indice)[2];
			String[] laNode = { mot, "" + (hauteur + 1) };
			node.add(laNode);

			aTrier.remove(indice);
		}

		for (int i = 0; i < aTrier.size(); i++) {
			touteRelation.add(aTrier.get(i));
		}
	}

	/**
	 * 
	 */
	private void affichage() {
		/* Création du graph */
		for (int i = 0; i < touteRelationAffi.size(); i++) {
			String[] laRelation = touteRelationAffi.get(i);

			if (laRelation[0].contains(">")) {
				laRelation[0] = raff(laRelation[0]);
			}

			if (laRelation[2].contains(">")) {
				laRelation[2] = raff(laRelation[2]);
			}

			Edge arcCourant;
			if (laRelation[1].equals("r_associated")) {
				arcCourant = graph.addRelation(laRelation[0], laRelation[1] + " " + laRelation[3], laRelation[2]);
			} else {
				arcCourant = graph.addRelation(laRelation[0], laRelation[1] + " " + laRelation[3], laRelation[2]);
				if (arcCourant != null) {
					arcCourant.addAttribute("ui.label", laRelation[1] + " " + laRelation[3]);
				}
			}

			System.out.println(
					laRelation[0] + " - " + laRelation[1] + " " + tailleEdge(laRelation[3]) + " - " + laRelation[2]);

			if (arcCourant != null) {
				GraphJDM.addClass(arcCourant, laRelation[1]);
				arcCourant.addAttribute("ui.style", "size : " + tailleEdge(laRelation[3]) + ";");
				// arcCourant.addAttribute("ui.style", "arrow-size : " +
				// tailleEdge(laRelation[3]) + ";");
			}
		}

		touteRelationAffi = new ArrayList<String[]>();
	}

	/**
	 * Renvoi une forme non formaté d'un rafinement initial mot>idRaff final
	 * mot>raff
	 * 
	 * @param leMot
	 *            rafiné
	 * @return un mot lisible
	 */
	private String raff(String leMot) {
		Mot mot = systeme.requete(leMot, Filtre.FiltreRelationsEntrantesSortantes);
		if (mot == null) {
			return leMot;
		} // else
		return mot.getMotFormate();
	}

	/**
	 * Renvoi une taille afin de formaté les arretes
	 * 
	 * @param poid
	 *            le poid de la relation
	 * @return le taille de l'arête
	 */
	private static String tailleEdge(String poid) {
		int max = 200;
		double taille = Math.abs(Double.parseDouble(poid)) + 0.5;
		String tailleS = taille > max ? "3,0" : "" + (taille * (3.0 / max));
		return tailleS.replace(".", ",");
	}

	/**
	 * insert les relations de raffinement si la node de départ et d'arrivée
	 * existe
	 */
	private void insertionRaff_ideeAssoc() {

		System.out.println("Nb raff " + raffRelation.size());
		for (int i = 0; i < raffRelation.size(); i++) {

			String[] tempo = raffRelation.get(i);

			if (tempo[0].contains(">")) {
				tempo[0] = raff(tempo[0]);
			}

			if (tempo[2].contains(">")) {
				tempo[2] = raff(tempo[2]);
			}

			if (graph.isNode(tempo[0]) && graph.isNode(tempo[2])) {
				touteRelationAffi.add(tempo);
				System.out.println(tempo[0] + " " + tempo[1] + " " + tempo[2]);
			}
		}

		if (filtres.isIdeeAssoc()) {
			System.out.println("Nb relation assoc " + ideeAssocRelation.size());
			for (int i = 0; i < ideeAssocRelation.size(); i++) {

				String[] tempo = ideeAssocRelation.get(i);

				if (graph.isNode(tempo[0]) && graph.isNode(tempo[2])) {
					touteRelationAffi.add(tempo);
					// System.out.println(tempo[0] + " " + tempo[1] + " " +
					// tempo[2]);
				}
			}
		}
	}
}