/*
 * Click.java                                18 juin 2018
 * Stagiaire LIRMM 2018
 */
package source.save19_06_18;

import java.util.ArrayList;
import java.util.HashMap;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.ViewerListener;
import org.graphstream.ui.view.ViewerPipe;

import requeterRezo.Mot;
import requeterRezo.RequeterRezo;
import requeterRezo.Voisin;

/**
 * TODO commenter la responsabilité de la classe
 * 
 * @author Thomas GEORGES
 *
 */
public class Clicks implements ViewerListener {
	protected boolean loop = true;
	protected ViewerPipe fromViewer;
	public static void main(String args[]) throws problemeRelationException {
		new Clicks();
	}

	private static String tailleEdge(String poid) {
		double taille = Math.abs(Double.parseDouble(poid));
		String tailleS = taille > 500 ? "3.0" : "" + taille * 0.006;
		return tailleS.replace(",", ".");
	}

	public Clicks() throws problemeRelationException {
		// We do as usual to display a graph. This
		// connect the graph outputs to the viewer.
		// The viewer is a sink of the graph.
		Graph graph = new SingleGraph("Clicks");

		String PATH_CSS = "style.css";

		graph.addAttribute("ui.quality");
		graph.addAttribute("ui.antialias");
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		graph.addAttribute("ui.stylesheet", "url('file:" + PATH_CSS + "')");

		graph.setAutoCreate(true);
		graph.setStrict(false);

		Viewer viewer = graph.display();
		// The default action when closing the view is to quit
		// the program.
		viewer.setCloseFramePolicy(Viewer.CloseFramePolicy.HIDE_ONLY);

		// We connect back the viewer to the graph,
		// the graph becomes a sink for the viewer.
		// We also install us as a viewer listener to
		// intercept the graphic events.
		fromViewer = viewer.newViewerPipe();
		fromViewer.addViewerListener(this);
		fromViewer.addSink(graph);

		// Then we need a loop to do our work and to wait for events.
		// In this loop we will need to call the
		// pump() method before each use of the graph to copy back events
		// that have already occurred in the viewer thread inside
		// our thread.

		/**********************************/

		// les entrée
		/*
		 * System.out.println("Saisissez un mot puis un id de relation");
		 * Scanner ee = new Scanner(System.in); String leMot = ee.nextLine();
		 * int laRelation = ee.nextInt();
		 */

		String leMot = "chaton";
		int laRelation = 6;

		// les REQUETES
		RequeterRezo systeme = new RequeterRezo("36h", 3000);
		ListeTypeRelation listeRelation = new ListeTypeRelation();

		Mot mot = systeme.requete("chaton");
		if (mot == null) {
			System.err.println("JDM est KO");
			return;
		}
		// HashMap avec <relation, ArrayListeDeMot>
		HashMap<String, ArrayList<Voisin>> listeEntree = new HashMap<String, ArrayList<Voisin>>();
		listeEntree = mot.getRelations_sortantes();

		ArrayList<String[]> listeNoeud = new ArrayList<String[]>();
		ArrayList<Voisin> listeRelationType = new ArrayList<Voisin>();

		for (int i = 0; i < listeRelation.getNbRelation(); i++) {
			listeRelationType = listeEntree.get(listeRelation.getRelByNum(i));
			if (listeRelationType != null && i == laRelation) {
				for (int u = 0; u < listeRelationType.size(); u++) {
					String m1 = mot.getNom();
					String m2 = listeRelationType.get(u).getNom();
					String rel = listeRelation.getRelByNum(i);
					String w = Double.toString(listeRelationType.get(u).getPoids());

					// mot1, t, mot2, poid
					String[] tempo = { m1, rel, m2, w };
					listeNoeud.add(tempo);
				}
			}
		}
		// Opération nécessaire une fois toutes les opérations réalisées.
		systeme.sauvegarder();

		/* partie graphe */
		System.out.println("graph");

		graph.addNode(mot.getNom());

		for (int i = 0; i < 10; i++) {
			String edgePK = (listeNoeud.get(i)[1] + listeNoeud.get(i)[0] + listeNoeud.get(i)[2]).replaceAll(" ", "");
			Edge arc = graph.addEdge(edgePK, listeNoeud.get(i)[0], listeNoeud.get(i)[2], true);

			System.out.println(edgePK);
			System.out.println(graph.getNode(mot.getNom()) != null);
			if (arc != null) {
				arc.addAttribute("ui.label", listeNoeud.get(i)[1] + " " + listeNoeud.get(i)[3]);
				// arc.addAttribute("ui.style", "size : "+ tailleEdge(""+i*50)
				// +";");
				System.out.println(tailleEdge("" + i * 50));
			}
		}

		for (Node node : graph) {
			node.addAttribute("ui.label", node.getId());
			if (node.getId().equals(leMot)) {
				node.setAttribute("ui.class", "racine");
			}
		}

		/**********************************/

		while (loop){
			fromViewer.pump(); // or fromViewer.blockingPump(); in the nightly
								// builds

			// here your simulation code.

			// You do not necessarily need to use a loop, this is only an
			// example.
			// as long as you call pump() before using the graph. pump() is non
			// blocking. If you only use the loop to look at event, use
			// blockingPump()
			// to avoid 100% CPU usage. The blockingPump() method is only
			// available from
			// the nightly builds.

		}
	}
	
	public void attente(){
		while (loop){
			fromViewer.pump();
		}
	}
	
	public void viewClosed(String id) {
		loop = false;
		System.out.println("LOL");
	}

	public void buttonPushed(String id) {
		System.out.println("Button pushed on node " + id);
	}

	public void buttonReleased(String id) {
		System.out.println("Button released on node " + id);
	}
}
