/*
 * Relation.java                                5 juin 2018
 * Stagiaire LIRMM 2018
 */

package source.save13_06_18;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * Création de la liste des relations de JDM
 * 
 * @author Thomas GEORGES
 *
 */
public class ListeTypeRelation {

	/** Tableau contenant les type des relation */
	private ArrayList<String> relation;
	/** Tableau contenant les id des relations */
	private ArrayList<Integer> idRel;
	/** Nombre de relations */
	private int nbRelation;
	
	/**
	 * Constructeur de la classe
	 * @throws problemeRelationException exception si la liste est mal généré / fichier inexistant
	 */
	public ListeTypeRelation() throws problemeRelationException {
		relation = new ArrayList<String>();
		idRel = new ArrayList<Integer>();
		nbRelation = 0;
		
		// rempli le tableau avec toutes les relations de JDM
		remplirRelation();
	}

	/**
	 * Lecture du fichier contenant les relations et remplissage des tableaux relation et idRel
	 * @throws problemeRelationException exception si la liste est mal généré / fichier inexistant
	 */
	private void remplirRelation() throws problemeRelationException {
		
		String path = "relationFormatee.txt";
		try (BufferedReader lecteur = new BufferedReader(new FileReader(path))) {
			String line = "";

			while ((line = lecteur.readLine()) != null) {
				String[] laRelation = line.split(" ");
				relation.add(laRelation[1]);
				idRel.add(Integer.parseInt(laRelation[0]));
				nbRelation++;
			}

		} catch (Exception e) {
			System.out.println("Erreur : " + e);
			throw new problemeRelationException();
		}
	}

	/**
	 * créer le fichier avec les relations
	 */
	private void generationFichierRelation() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Associe id et relation
	 * @param id de la relation
	 * @return la relation associé
	 */
	public String getRelById(int id) {
		return idRel.contains(id) ? relation.get(idRel.indexOf(id)) : "0";
	}

	/**
	 * Associe indice et relation
	 * @param num indice du tableau de la relation
	 * @return la relation associé
	 */
	public String getRelByNum(int num) {
		return nbRelation > num ? relation.get(num) : "0";
	}
	
	/**
	 * Associe id et relation
	 * @param rel la relation
	 * @return  l'id de la relation associé
	 */
	public int getIDByRel(String rel) {
		return relation.contains(rel) ? idRel.get(relation.indexOf(rel)) : 0;
	}
	
	/**
	 * @return le nombre de relation contenue dans la table
	 */
	public int getNbRelation(){
		return nbRelation;
	}
}
