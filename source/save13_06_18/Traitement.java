/*
 * Traitement.java                                7 juin 2018
 * Stagiaire LIRMM 2018
 */
package source.save13_06_18;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;

import requeterRezo.Filtre;
import requeterRezo.Mot;
import requeterRezo.RequeterRezo;
import requeterRezo.Voisin;

/**
 * Traitement et mise en relation des données Puis affichage et gestion du
 * graphe
 * 
 * @author Thomas GEORGES
 *
 */
public class Traitement {
	/** Objet manipulant les filtres */
	private Filtres filtres;
	/** Objet contenant la structure du graphe de JDM */
	private GraphJDM graph;
	/** Requêtteur de rezoJDM */
	private RequeterRezo systeme;
	/** Structure avec la liste des types de relations de rezoJDM */
	private ListeTypeRelation listeRelation;
	/** tableau contenant les relations à afficher format (n1, t, n2, w) */
	private ArrayList<String[]> relation;
	/** tableau contenant les raffinement format (n1, t, n2, w) */
	private ArrayList<String[]> raffRelation;
	/**
	 * Indice permettant de savoir où l'on s'es arreté dans l'affichage du
	 * tableau relation
	 */
	private int indiceCourantRelation;

	/**
	 * traitement et mise en relation des données et du graphe
	 * 
	 * @param filtres
	 *            la structure contenant les filtres
	 * @throws problemeRelationException
	 *             Si la liste des types de relations est mal généré
	 * @throws ExceptionJDM
	 *             Si problème sur le requêtteur
	 * @throws MalformedURLException
	 *             Problème d'URL
	 * @throws IOException
	 *             Problème d'affichage
	 * @throws InterruptedException
	 *             Problème ?
	 */
	public Traitement(Filtres filtres)
			throws problemeRelationException, ExceptionJDM, MalformedURLException, IOException, InterruptedException {

		System.out.println("Traitement : ");

		/* Initialisations */
		relation = new ArrayList<String[]>();
		raffRelation = new ArrayList<String[]>();
		this.filtres = filtres;
		listeRelation = new ListeTypeRelation();
		graph = new GraphJDM(filtres.getTerme());
		indiceCourantRelation = 0;

		systeme = new RequeterRezo("36h", 3000);

		/* Affichage des filtres */
		System.out.println("\n\tFiltres :");
		System.out.println("Mot : " + filtres.getTerme());
		System.out.println("Relations " + (filtres.isRelationEntrante(0) ? "sortante" : "entrante"));
		String[] tab1 = filtres.getRelationHauteur();
		int[] tab = filtres.getNbVoisin();
		for (int i = 0; i < filtres.getNb_nbVoisin(); i++) {
			System.out.println("Voisin de distance " + (i + 1) + " : " + tab[i] + " avec la relation "
					+ listeRelation.getRelById(Integer.parseInt(tab1[i])));
		}

		/* Récupération des relations */
		Mot racine = null;
		racine = systeme.requete(filtres.getTerme());
		if (racine == null) {
			throw new ExceptionJDM();
		}

		Node rac = graph.addRacine(filtres.getTerme());
		GraphJDM.addClass(rac, "racine");

		System.out.println("\n\tRelation :");
		/* Rempli le tableau des relations */
		remplirRelation(racine, filtres.getNbVoisinIndice(0), 0);

		System.out.println("\n\t Affichage");
		affichage();

		/* ajoute au tableau des relations les relations de raffinement utile */
		System.out.println("\n Rafinement");
		verifSiRaffRelation();

		System.out.println("\n\t Affichage");
		affichage();

		System.out.println("\t Display");
		graph.afficher();

		systeme.sauvegarder();
	}

	/**
	 * Remplissage du tableau relation à afficher à partir d'un mot et de sa
	 * hauteur
	 * 
	 * @param mot
	 *            mot racine
	 * @param nb
	 *            nombre de relation à ajouter au tableau
	 * @param hauteur
	 *            le niveau du graphe racine = 0 relation 1er ordre = 1 ...
	 */
	private void remplirRelation(Mot mot, int nb, int hauteur) {

		HashMap<String, ArrayList<Voisin>> listeEntree = new HashMap<String, ArrayList<Voisin>>();
		if (filtres.isRelationEntrante(hauteur)) {
			listeEntree = mot.getRelations_sortantes();
		} else {
			listeEntree = mot.getRelations_entrantes();
		}

		ArrayList<String[]> listeNoeud = new ArrayList<String[]>();
		ArrayList<String[]> listeNoeudIdee = new ArrayList<String[]>();
		ArrayList<Voisin> listeRelationType = new ArrayList<Voisin>();

		/* Remplissage du tableau en fonction des relations */
		for (int i = 0; i < listeRelation.getNbRelation(); i++) {

			listeRelationType = listeEntree.get(listeRelation.getRelByNum(i));

			if (listeRelationType != null
					&& (i == Integer.parseInt(filtres.getRelation(hauteur)) || (i == 1) || (i == 0))) {

				for (int u = 0; u < listeRelationType.size(); u++) {

					String m1 = mot.getMotFormate();
					String m2 = listeRelationType.get(u).getNom();

					if (m2.contains(">")) {
						m2 = raff(m2);
					}

					String rel = listeRelation.getRelByNum(i);
					String w = Double.toString(listeRelationType.get(u).getPoids());

					if (!filtres.isRelationEntrante(hauteur)) {
						String temp = m1;
						m1 = m2;
						m2 = temp;
					}

					// mot1, t, mot2, poid
					String[] tempo = { m1, rel, m2, w };
					if ((i == 1) && (Integer.parseInt(filtres.getRelation(hauteur)) != 1)) {
						raffRelation.add(tempo);
					} else if ((i == 0) && (Integer.parseInt(filtres.getRelation(hauteur)) != 1)) {
						listeNoeudIdee.add(tempo);
					} else {
						listeNoeud.add(tempo);
					}
				}
			}
		}

		/*
		 * trie des nb relation par ordre decroissant du poid en valeur absolue
		 */
		for (int i1 = 0; i1 < nb && listeNoeud.size() > 0; i1++) {
			String[] max = { "", "", "", "" + Double.MIN_VALUE };
			String[] maxIdee = { "", "", "", "" + Double.MIN_VALUE };
			int indice = 0;
			int indiceIdee = 0;

			for (int u = 0; u < listeNoeud.size(); u++) {
				if ((Math.abs(Double.parseDouble(max[3])) < Math.abs(Double.parseDouble(listeNoeud.get(u)[3])))) {
					max = listeNoeud.get(u);
					indice = u;
				} // else
			}

			for (int u = 0; u < listeNoeudIdee.size(); u++) {
				if ((Math.abs(Double.parseDouble(max[3])) < Math.abs(Double.parseDouble(listeNoeudIdee.get(u)[3])))) {
					maxIdee = listeNoeudIdee.get(u);
					indiceIdee = u;
				} // else
			}

			if(indice > 0){
				listeNoeud.remove(indice);
				relation.add(max);
			}
			
			/*
			 * Insertion et appel récursif
			 */
			if (hauteur+1 < filtres.getNb_nbVoisin() && (indice > 0) && max[2].charAt(0) != '_' && max[0].charAt(0) != '_') {
				

				Mot motCourant;
				try {
					if (filtres.isRelationEntrante(hauteur)) {
						System.out.println("hauteur : " + (hauteur) + " du mot " + max[2]);
						motCourant = systeme.requete(max[2]);
					} else {
						System.out.println("hauteur : " + (hauteur) + " du mot " + max[0]);
						motCourant = systeme.requete(max[0]);
					}

					if (motCourant != null) {
						remplirRelation(motCourant, filtres.getNbVoisinIndice(hauteur), hauteur + 1);
					}
				} catch (Exception e) {
					System.err.println(e + " ligne 229");
				}
			}

			if(indiceIdee > 0 && maxIdee[2].charAt(0) != '_' && maxIdee[0].charAt(0) != '_'){
				listeNoeudIdee.remove(indiceIdee);
				relation.add(maxIdee);
			}
			
			/*
			 * Idée associé
			 */
			if (hauteur+1 < filtres.getNb_nbVoisin() && indiceIdee > 0 && maxIdee[2].charAt(0) != '_'
					&& maxIdee[0].charAt(0) != '_') {

				Mot motCourantIdee = null;
				try {
					if (filtres.isRelationEntrante(hauteur)) {
						System.out.println("hauteur : " + (hauteur) + " du mot " + maxIdee[2] + "(idée assoc)");
						motCourantIdee = systeme.requete(maxIdee[2]);
					} else {
						System.out.println("hauteur : " + (hauteur) + " du mot " + maxIdee[0] + "(idée assoc)");
						motCourantIdee = systeme.requete(maxIdee[0]);
					}

					if (motCourantIdee != null) {
						remplirRelation(motCourantIdee, filtres.getNbVoisinIndice(hauteur), hauteur + 1);
					}
				} catch (Exception e) {
					System.err.println(e + " ligne 258");
				}
			}
		}
	}

	/**
	 * 
	 */
	private void affichage() {
		/* Création du graph */
		for (int i = indiceCourantRelation; i < relation.size(); i++) {
			String[] laRelation = relation.get(i);

			Edge arcCourant = graph.addRelation(laRelation[0], laRelation[1] + " " + laRelation[3], laRelation[2]);
			System.out.println(
					laRelation[0] + " - " + laRelation[1] + " " + tailleEdge(laRelation[3]) + " - " + laRelation[2]);

			if (arcCourant != null) {

				GraphJDM.addClass(arcCourant, laRelation[1]);
				arcCourant.addAttribute("ui.style", "size : " + tailleEdge(laRelation[3]) + ";");
			}
			if (arcCourant != null && laRelation[1].equals("r_associated")) {
				arcCourant.addAttribute("ui.style", "text-alignment: along;");
			}
			indiceCourantRelation++;
		}
	}

	/**
	 * Renvoi une forme non formaté d'un rafinement initial mot>idRaff final
	 * mot>raff
	 * 
	 * @param leMot
	 *            rafiné
	 * @return un mot lisible
	 */
	private String raff(String leMot) {
		Mot mot = systeme.requete(leMot, Filtre.FiltreRelationsEntrantesSortantes);
		if (mot == null) {
			return leMot;
		} // else
		return mot.getMotFormate();
	}

	/**
	 * Renvoi une taille afin de formaté les arretes
	 * 
	 * @param poid
	 *            le poid de la relation
	 * @return le taille de l'arête
	 */
	private static String tailleEdge(String poid) {
		int max = 200;
		double taille = Math.abs(Double.parseDouble(poid)) + 0.5;
		String tailleS = taille > max ? "3,0" : "" + (taille * (3.0 / max));
		return tailleS.replace(".", ",");
	}

	/**
	 * insert les relations de raffinement si la node de départ et d'arrivée
	 * existe
	 */
	private void verifSiRaffRelation() {

		for (int i = 0; i < raffRelation.size(); i++) {

			String[] tempo = raffRelation.get(i);

			if (graph.isNode(tempo[0]) && graph.isNode(tempo[2])) {
				relation.add(tempo);
				System.out.println(tempo[0] + "r_raff_sem" + tempo[2]);
			}
		}
	}
}
