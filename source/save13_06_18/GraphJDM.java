/*
 * GraphJDM.java                                6 juin 2018
 * Stagiaire LIRMM 2018
 */
package source.save13_06_18;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Element;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

/**
 * Permet de gérer la visualisation du graph basé sur rézoJDM
 * @author Thomas GEORGES 
 *
 */
public class GraphJDM {
	/** Objet venant de l'API graphStream */
	private Graph graph;
	/** Chemin vers le fichier CSS lié au graph */
	private String PATH_CSS = "style.css";
	/** Le sommet racine du graph */
	private Node racine;
	/** (id des arcs) Permet de créer des arcs uniques */
	private long idNode;
	
	/**
	 * Constructeur par defaut
	 * Appel à initialisation qui set toutes les propriétés du graphe
	 */
	public GraphJDM(){
		idNode = 0;
		graph = new SingleGraph("Graph");
		initialisation();
	}
	
	/** 
	 * Constructeur permettant de set le titre du graphe
	 * Appel à initialisation qui set toutes les propriétés du graphe
	 * @param titre le titre du graphe
	 */
	public GraphJDM(String titre){
		idNode = 0;
		graph = new SingleGraph(titre);
		initialisation();
	}
	
	/**
	 * Initialisation des propriétés du graphe
	 * - Sa qualité graphique
	 * - L'utilisation de l'antiAliasing
	 * - Les bibliothèques graphiques utilisées
	 * - La permissibilité de construction
	 * 		- La création automatique d'une node lors de la création d'un arc
	 * 
	 */
	private void initialisation(){
		graph.addAttribute("ui.quality");
		graph.addAttribute("ui.antialias");
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		graph.addAttribute("ui.stylesheet", "url('file:" + PATH_CSS + "')");

		graph.setAutoCreate(true);
		graph.setStrict(false);
	}
	
	/**
	 * @return l'objet graphe
	 */
	public Graph getGraph(){
		return graph;
	}
	
	/**
	 * @return la racine du graphe
	 */
	public Node getRacine(){
		return racine;
	}
	
	/**
	 * Création d'un arc orienté nommé t
	 * n1 -> n2
	 * @param n1 Node de départ
	 * @param t	type de relation
	 * @param n2 Node de destination
	 * @return L'objet arc ainsi crée
	 */
	public Edge addRelation(String n1, String t, String n2){
		Edge arc = graph.addEdge(""+idNode, n1, n2, true);
		if(arc != null){
			idNode++;
			arc.addAttribute("ui.label", t);
			graph.getNode(n2).addAttribute("ui.label", n2);
			graph.getNode(n1).addAttribute("ui.label", n1);
		}
		return arc;
	}
	
	/**
	 * Création d'un arc orienté nommé t partant de la racine
	 * rac -> n2
	 * @param t	type de relation
	 * @param n2 Node de destination
	 * @return L'objet arc ainsi crée
	 */
	public Edge addRelationRacineToNode(String t, String n2){
		Edge arc = graph.addEdge(""+idNode, racine.getId(), n2, true);
		idNode++;
		
		arc.addAttribute("ui.label", t);
		graph.getNode(n2).addAttribute("ui.label", n2);
		return arc;
	}
	
	/**
	 * Création d'un arc orienté nommé t ayant en destination la racine
	 * n1 -> rac
	 * @param t	type de relation
	 * @param n1 Node de départ
	 * @return L'objet arc ainsi crée
	 */
	public Edge addRelationNodeToRacine(String t, String n1){
		Edge arc = graph.addEdge(""+idNode, n1, racine.getId(), true);
		idNode++;
		
		arc.addAttribute("ui.label", t);
		graph.getNode(n1).addAttribute("ui.label", n1);
		return arc;
	}
	
	/**
	 * Création de la racine
	 * @param rac la racine
	 * @return L'objet Node associé à la racine
	 */
	public Node addRacine(String rac){
		racine = graph.addNode(rac);
		racine.addAttribute("ui.label", rac);
		racine.addAttribute("ui.class", rac);
		return racine;
	}
	
	/**
	 * Créer une classe pour un arc ou une node
	 * (Utile pour le CSS)
	 * @param arc_node L'objet où appliquer la class
	 * @param classe la classe à appliquer
	 */
	public static void addClass(Element arc_node, String classe){
		arc_node.setAttribute("ui.class", classe);
	}
	
	/**
	 * Affichage du graphe
	 */
	public void afficher(){
		graph.display();
	}

	/** TODO commenter le rôle de la méthode
	 * @param string
	 * @return
	 */
	public boolean isNode(String node) {
		return (graph.getNode(node) != null);
	}
}
