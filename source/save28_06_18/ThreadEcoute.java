/*
 * ThreadEcoute.java                                20 juin 2018
 * Stagiaire LIRMM 2018
 */
package source.save28_06_18;

/**
 * TODO commenter la responsabilité de la classe
 * 
 * @author Thomas GEORGES
 *
 */
public class ThreadEcoute extends Thread {
	GraphJDM graph;

	/**
	 * thread d'écoute
	 * @param graph graph sur lequel le thread fera son écoute
	 */
	ThreadEcoute(GraphJDM graph) {
		super("MonEcoute");
		this.graph = graph;
	}

	public void run() {
		graph.attente(true);
	}
}
	