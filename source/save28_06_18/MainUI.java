/*
 * Main.java                                11 juin 2018
 * Stagiaire LIRMM 2018
 */
package source.save28_06_18;

import java.awt.EventQueue;

/**
 * Lancement de l'application
 * @author Thomas GEORGES 
 *
 */
public class MainUI {

	/**
	 * Launch the application.
	 * @param args unused
	 */
	public static void main(String[] args) {
		System.out.println("Début Appli");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				Filtres filtres = new Filtres();
				try {

					InterfaceFiltres frame = new InterfaceFiltres(filtres);
					frame.setVisible(true);
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

}
