Développement d'un outil de visualisation de graphes basé sur le réseau lexical et sémantique rezoJDM. Réseau alimenté par le site JeuxDeMots développé par des enseignants-chercheurs du LIRMM.
Utilisation d'une API permettant d'effectuer des requêtes sur le réseau, puis génération de graphes dynamiques à partir d'une seconde API après traitement des données

Par Thomas GEORGES (Stage Licence 3e Année -> Master 1ere année:  
https://www.linkedin.com/in/thomas-georges-69b19a116/

JeuxDeMots :  
http://www.jeuxdemots.org/jdm-about.php

![Exemple de graphe](grapheLoup.jpg "grapheLoup")

Il est possible de lancer l'application via une commande :   
`java -jar visualisationGraph.jar`  
Afin d'obtenir de plus amples informations sur l'exécution du programme.

![L'interface](Fonctionnement.png "fonctionnement")
